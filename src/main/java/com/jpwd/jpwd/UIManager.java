package com.jpwd.jpwd;

import com.jpwd.jpwd.db.JPwdDB;
import javafx.scene.control.ListView;

import java.sql.SQLException;
import java.util.Map;
import java.util.prefs.Preferences;


public class UIManager {

    public void updateList(String decryptionKey, ListView<CustomPair<Integer,String>> listview) {
        Map<CustomPair<Integer,String>, CustomPair<String,String>> credentials = null;

        try {
            credentials = JPwdDB.getAllCredentials(decryptionKey);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        listview.getItems().clear();

        for (CustomPair<Integer, String> key:credentials.keySet()) {
            listview.getItems().add(key);
        }

    }

    static void storeMasterKey(byte[] masterKeyHash) {
        Preferences prefs = Preferences.userRoot().node("JPwd").node("password");
        prefs.putByteArray("masterKey",masterKeyHash);
    }

    static byte[] getMasterKey() {
        Preferences prefs = Preferences.userRoot().node("JPwd").node("password");
        return prefs.getByteArray("masterKey",null);
    }

}
