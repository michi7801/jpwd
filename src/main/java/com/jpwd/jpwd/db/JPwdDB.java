package com.jpwd.jpwd.db;

import com.jpwd.jpwd.CustomPair;
import com.jpwd.jpwd.encrypt.JPwdAES;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class JPwdDB {
    static Connection conn;
    static Statement stmt;

    public static void connectionToDerby() throws SQLException {
        // -------------------------------------------
        // URL format is
        // jdbc:derby:<local directory to save data>
        // -------------------------------------------
        String dbUrl = "jdbc:derby:JPwd;create=true";
        conn = DriverManager.getConnection(dbUrl);
        stmt = conn.createStatement();

    }

    public static void createDbIfNotExists() {

        // create table
        try {
            stmt.executeUpdate("Create table users (ID int primary key GENERATED ALWAYS AS IDENTITY, PASSWD varchar(256), USERNAME varchar(64), DESCRIPTION varchar(256))");
        } catch (SQLException ignored) { }

    }

    public static void setCredentials(String passwd, String username, String description, String encryptionkey) throws Exception {
        //Encrypt
        passwd=JPwdAES.encrypt(passwd, encryptionkey);

        String prep= "insert into users(PASSWD,USERNAME, DESCRIPTION) values (?,?,?)";
        PreparedStatement prep1 = conn.prepareStatement(prep);
        prep1.setString(1,passwd);
        prep1.setString(2,username);
        prep1.setString(3,description);

        prep1.executeUpdate();

    }


    public static void deleteCredentials(Integer id, String description) throws Exception {

        String prep= "delete from users where ID=? and DESCRIPTION=?";
        PreparedStatement prep1 = conn.prepareStatement(prep);
        prep1.setInt(1,id);
        prep1.setString(2,description);

        prep1.executeUpdate();

    }

    public static CustomPair<String, String> getCredentials(int myid, String decryptionkey) throws SQLException {
        String prep= "SELECT * FROM users WHERE ID=?";
        PreparedStatement prep1 = conn.prepareStatement(prep);
        prep1.setInt(1,myid);

        ResultSet rs = prep1.executeQuery();
        while (rs.next()) {
            String decryptPasswd = null;
            try {
                decryptPasswd = JPwdAES.decrypt(rs.getString("passwd"),decryptionkey);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return new CustomPair<>(decryptPasswd, rs.getString("username"));
        }

         throw new SQLDataException("No Credentials found");
    }

    public static Map<CustomPair<Integer,String>, CustomPair<String,String>> getAllCredentials(String decryptionkey) throws SQLException {
        String prep= "SELECT * FROM users";
        PreparedStatement prep1 = conn.prepareStatement(prep);
        ResultSet rs = prep1.executeQuery();

        Map<CustomPair<Integer,String>,CustomPair<String,String>> credentials = new HashMap<>();

        while (rs.next()) {
            String decryptPasswd = null;
            try {
                decryptPasswd = JPwdAES.decrypt(rs.getString("passwd"),decryptionkey);
            } catch (Exception e) {
                e.printStackTrace();
            }

                credentials.put(new CustomPair<>(rs.getInt("ID"),rs.getString("DESCRIPTION")),new CustomPair<>(decryptPasswd, rs.getString("username"))

            );
        }

        return credentials;
    }



}
