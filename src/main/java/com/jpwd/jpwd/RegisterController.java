package com.jpwd.jpwd;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;

public class RegisterController implements Initializable {

    @FXML
    TextField passwordTextField;

    @FXML
    TextField repeatTextField;

    @FXML
    Button setPassword;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setPassword.setOnAction(e -> {
            String repeat = repeatTextField.getText();
            String password = passwordTextField.getText();

            repeatTextField.clear();
            passwordTextField.clear();

            if (repeat.equals(password)) {
                MasterKey.setMasterKey(password);
                String salt = "jpwd";
                try {
                    MessageDigest md = MessageDigest.getInstance("SHA-512");
                    md.update(salt.getBytes());
                    byte[] hashedPassword = md.digest(password.getBytes(StandardCharsets.UTF_8));
                    UIManager.storeMasterKey(hashedPassword);

                    Stage stage = (Stage) setPassword.getScene().getWindow();
                    BorderPane main = FXMLLoader.load(getClass().getResource("/main.fxml"));
                    Scene scene = new Scene(main, 800,420);
                    stage.setScene(scene);

                } catch (NoSuchAlgorithmException | IOException noSuchAlgorithmException) {
                    noSuchAlgorithmException.printStackTrace();
                }
            } else {
                repeatTextField.setStyle("-fx-text-box-border: red; -fx-focus-color: red");
            }
        });
    }
}
