package com.jpwd.jpwd;

import javafx.beans.NamedArg;

import java.io.Serializable;
import java.util.Objects;

public class CustomPair<K, V> implements Serializable {
    @Override
    public String toString() {
        return ""+getValue();
    }

    private final K key;
    private final V value;

    public K getKey() {
        return this.key;
    }

    public V getValue() {
        return this.value;
    }

    public CustomPair(@NamedArg("key") K var1, @NamedArg("value") V var2) {
        this.key = var1;
        this.value = var2;
    }

    public int hashCode() {
        byte var1 = 7;
        int var2 = 31 * var1 + (this.key != null ? this.key.hashCode() : 0);
        var2 = 31 * var2 + (this.value != null ? this.value.hashCode() : 0);
        return var2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomPair<?, ?> that = (CustomPair<?, ?>) o;
        return Objects.equals(key, that.key) &&
                Objects.equals(value, that.value);
    }
}
