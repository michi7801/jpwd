package com.jpwd.jpwd;

import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class AddNewPopUp {

    private DialogPane root;

    private TextField usernameField;
    private TextField passwordField;
    private TextField descriptionField;

    void construct() {
        root = new DialogPane();
        VBox vbox = new VBox();

        vbox.setPrefHeight(100);
        vbox.setPrefWidth(200);

        Label usernameLabel = new Label("Username");
        usernameField = new TextField();

        Label passwordLabel = new Label("Password");
        passwordField = new TextField();

        Label descriptionLabel = new Label("Description");
        descriptionField = new TextField();

        vbox.getChildren().addAll(descriptionLabel, descriptionField);
        vbox.getChildren().addAll(usernameLabel, usernameField);
        vbox.getChildren().addAll(passwordLabel, passwordField);

        root.setContent(vbox);
    }


    public DialogPane getRoot() {
        return root;
    }

    public TextField getUsernameField() {
        return usernameField;
    }

    public TextField getPasswordField() {
        return passwordField;
    }

    public TextField getDescriptionField() {
        return descriptionField;
    }
}
