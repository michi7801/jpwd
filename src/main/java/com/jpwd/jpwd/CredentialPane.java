package com.jpwd.jpwd;

import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class CredentialPane {
    private DialogPane root;
    private Label customUsername;
    private Label customPassword;

    void construct() {
        root = new DialogPane();
        VBox rootLayout = new VBox();


        rootLayout.setPrefHeight(100);
        rootLayout.setPrefWidth(230);

        root.setPrefWidth(250);
        root.setPrefHeight(80);

        HBox userContainer = new HBox();
        HBox passwordContainer = new HBox();

        Label user = new Label("Username: ");
        Label password = new Label("Password: ");

        customUsername = new Label();
        customPassword = new Label();

        userContainer.getChildren().addAll(user,customUsername);
        passwordContainer.getChildren().addAll(password,customPassword);

        rootLayout.getChildren().addAll(userContainer,passwordContainer);
        root.setContent(rootLayout);
    }

    public Label getCustomUsername() {
        return customUsername;
    }

    public Label getCustomPassword() {
        return customPassword;
    }

    public DialogPane getRoot() {
        return root;
    }

    public void setCustomUsername(String customUsername) {
        this.customUsername.setText(customUsername);
    }

    public void setCustomPassword(String customPassword) {
        this.customPassword.setText(customPassword);
    }
}
