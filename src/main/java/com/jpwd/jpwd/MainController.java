package com.jpwd.jpwd;

import com.jpwd.jpwd.db.JPwdDB;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    final UIManager uiManager = new UIManager();
    final AddNewPopUp addNewPopUp = new AddNewPopUp();
    final CredentialPane credentialPane = new CredentialPane();
    final Alert popOver = new Alert(Alert.AlertType.CONFIRMATION);
    final Alert showCred = new Alert(Alert.AlertType.INFORMATION);

    @FXML
    ListView<CustomPair<Integer,String>> mainList;

    @FXML
    Button newButton;

    @FXML
    Button deleteButton;

    public void insertNewCredential() {
        popOver.showAndWait();
    }

    public void deleteCredential() {
        CustomPair<Integer,String> selected = mainList.getSelectionModel().getSelectedItem();
        try {
            JPwdDB.deleteCredentials(selected.getKey(),selected.getValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
        uiManager.updateList(MasterKey.getMasterKey(), mainList);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        uiManager.updateList(MasterKey.getMasterKey(), mainList);
        addNewPopUp.construct();
        credentialPane.construct();
        popOver.setDialogPane(addNewPopUp.getRoot());
        popOver.getButtonTypes().addAll(ButtonType.OK);

        showCred.setHeight(100);
        showCred.setWidth(200);
        showCred.setDialogPane(credentialPane.getRoot());
        showCred.getButtonTypes().addAll(ButtonType.OK);
        //showCred.setContentNode(credentialPane.getRoot());

        addButton();
        showCredentials();

        popOver.setOnHiding(e -> addButton());

    }

    public void logout() throws IOException {
        MasterKey.setMasterKey(null);

        Stage stage = (Stage) deleteButton.getScene().getWindow();
        BorderPane main = FXMLLoader.load(getClass().getResource("/login.fxml"));
        Scene scene = new Scene(main, 800,420);
        stage.setScene(scene);

    }

    private void addButton() {

            String passwd = addNewPopUp.getPasswordField().getText();
            String username = addNewPopUp.getUsernameField().getText();
            String desc = addNewPopUp.getDescriptionField().getText();

            if (desc != null && !desc.equals("") && !desc.equals(" "))
            {
                try {
                    JPwdDB.setCredentials(passwd,username,desc,MasterKey.getMasterKey());
                } catch (Exception exception) {
                    exception.printStackTrace();
                }

                uiManager.updateList(MasterKey.getMasterKey(), mainList);
                addNewPopUp.getPasswordField().clear();
                addNewPopUp.getUsernameField().clear();
                addNewPopUp.getDescriptionField().clear();
            }
    }

    private void showCredentials() {
        mainList.setOnMouseClicked(event -> {
            if(event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                showCred.setTitle("Credentials");

                try {
                    CustomPair<Integer, String> selected =  mainList.getSelectionModel().getSelectedItem();
                    CustomPair<String,String> credToShow = JPwdDB.getCredentials(selected.getKey(),MasterKey.getMasterKey());

                    credentialPane.setCustomPassword(credToShow.getKey());
                    credentialPane.setCustomUsername(credToShow.getValue());
                    showCred.showAndWait();

                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

            }
        });
    }
}
