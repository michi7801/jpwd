package com.jpwd.jpwd;

public class MasterKey {
    private static String masterKey = "";

    static void setMasterKey(String masterKey) {
        MasterKey.masterKey = masterKey;
    }

    static String getMasterKey() {
        return masterKey;
    }
}
