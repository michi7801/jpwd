package com.jpwd.jpwd;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.ResourceBundle;

public class LoginController implements Initializable {
    @FXML
    Button loginButton;

    @FXML
    TextField masterPassword;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loginButton.setOnAction(e -> {
            try {
                String passwd = masterPassword.getText();
                masterPassword.clear();
                MasterKey.setMasterKey(passwd);

                String salt = "jpwd";
                MessageDigest md = MessageDigest.getInstance("SHA-512");
                md.update(salt.getBytes());
                byte[] hashedPassword = md.digest(passwd.getBytes(StandardCharsets.UTF_8));

                if (UIManager.getMasterKey() != null && Arrays.equals(UIManager.getMasterKey(),hashedPassword)) {
                    masterPassword.setStyle(null);
                    BorderPane main = FXMLLoader.load(getClass().getResource("/main.fxml"));
                    Scene scene = new Scene(main, 800,420);
                    Stage stage = (Stage) loginButton.getScene().getWindow();
                    stage.setScene(scene);
                }
                else {
                    masterPassword.setStyle("-fx-text-box-border: red; -fx-focus-color: red");
                }

            } catch (IOException | NoSuchAlgorithmException ioException) {
                ioException.printStackTrace();
            }
        });
    }
}
