package com.jpwd.jpwd;

import com.jpwd.jpwd.db.JPwdDB;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.prefs.Preferences;

public class Main extends Application  {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException, SQLException {

        JPwdDB.connectionToDerby();
        JPwdDB.createDbIfNotExists();

        primaryStage.setTitle("JPwd");

        //clearPrefs();

        BorderPane login;
        if (UIManager.getMasterKey() == null || UIManager.getMasterKey().length == 0) {
            login = FXMLLoader.load(getClass().getResource("/register.fxml"));
        } else {
            login = FXMLLoader.load(getClass().getResource("/login.fxml"));
        }

        primaryStage.setScene(new Scene(login, 800, 420));
        primaryStage.show();

    }

    public void clearPrefs() {
        Preferences prefs = Preferences.userRoot().node("JPwd").node("password");
        prefs.putByteArray("masterKey",new byte[0]);
    }


}
