module jpwd {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires java.prefs;

    exports com.jpwd.jpwd;
    opens com.jpwd.jpwd;
}